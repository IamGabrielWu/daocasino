pragma solidity ^0.4.21;
contract FootballLottory {
  address public casino_manager;

// currently solidity doesn't support ufixed.
// to do find a way to change rate to ufixed or float or double type.
  struct Rule {
    string playera;
    string playerb;
    uint rate_for_playera;
    uint rate_for_playerb;
    uint rate_for_draw;
    uint aScores;
    uint bScores;
    string summary;
    bool isFinalized;
  }
  enum ResultType {aOverb,bOvera,draw}
  Rule[] public rules;
  struct Deal {
    address gambler;
    uint money;
    ResultType gamblefor;
  }
  mapping (uint => Deal[]) deals;

  modifier onlycasino() {
    require(msg.sender==casino_manager);
    _;
  }
  function FootballLottory( ) {
    casino_manager=msg.sender;
  }

  function createRule(string playera, string playerb, uint rate_for_playera,
    uint rate_for_playerb, uint rate_for_draw, string summary) public onlycasino {
    require(rate_for_playera<100 && rate_for_playera >0 && rate_for_playerb <100 && rate_for_playerb >0 && rate_for_draw<100 && rate_for_draw>0);
    Rule memory rule=Rule(
        playera,
        playerb,
        rate_for_playera,
        rate_for_playerb,
        rate_for_draw,
        0,
        0,
        summary,
        false
    );
      rules.push(rule);
  }

  function gamble(uint gamblefor) public payable {
    uint amount=msg.value;
    Deal[] deallist=deals[gamblefor]; // maybe by default mapping (uint => Deal[]) deals will have a initialized value Deal[] with length == 0
    deallist.push(Deal(msg.sender, msg.value,ResultType(gamblefor)));
  }

//TO DO for loop wastes gas.
  function finalizeBet(uint aScore, uint bScore,uint ruleId) public onlycasino{
    Rule storage rule=rules[ruleId];
    require(rule.isFinalized==false);
    if(aScore>bScore){
      Deal[] storage winnersA=deals[uint(ResultType.aOverb)];
      for(uint i=0;i<winnersA.length;i++){
        Deal memory dealA=winnersA[i];
        dealA.gambler.transfer(dealA.money*(1+rule.rate_for_playera/100));
      }
    }else if(bScore>aScore){
      Deal[] storage winnersB=deals[uint(ResultType.bOvera)];
      for(uint j=0;j<winnersB.length;j++){
        Deal memory dealB=winnersB[j];
        dealB.gambler.transfer(dealB.money*(1+rule.rate_for_playerb/100));
      }
    }else{
      Deal[] storage winnersC=deals[uint(ResultType.draw)];
      for(uint k=0;i<winnersC.length;k++){
        Deal memory dealC=winnersC[k];
        dealC.gambler.transfer(dealC.money*(1+rule.rate_for_draw/100));
      }
    }
    rule.isFinalized=true;
  }
}
